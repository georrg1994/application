package com.example.application.viewmodels

import androidx.lifecycle.ViewModel
import com.example.application.repositories.Repository

open class BaseViewModel : ViewModel() {
    var loadingState = Repository.loadingState
}