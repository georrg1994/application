package com.example.application.viewmodels

import androidx.lifecycle.MutableLiveData
import com.example.application.helpers.Mode
import com.example.application.models.Autocomplete
import com.example.application.models.City
import com.example.application.repositories.Repository
import kotlinx.coroutines.*
import kotlinx.coroutines.android.Main

private const val DELAY_BEFORE_AUTOCOMPLETE_REQUEST = 800L

class TripViewModel : BaseViewModel() {
    private var delayBeforeRequest: Job? = null

    var startCity: MutableLiveData<City> = MutableLiveData()
    var endCity: MutableLiveData<City> = MutableLiveData()
    var lastTem: String? = null

    fun getAutocomplete(term: String = "", lang: String = "en"): MutableLiveData<Autocomplete> {
        if (term.isNotEmpty() && lastTem != term)
            lastTem = term

        return Repository.getAutocomplete(term, lang)
    }

    fun safeAutocompleteUpdate(query: String, lang: String = "en") {
        delayBeforeRequest?.cancel()
        delayBeforeRequest = GlobalScope.launch(context = Dispatchers.Main) {
            delay(DELAY_BEFORE_AUTOCOMPLETE_REQUEST)
            getAutocomplete(query, lang)
        }
    }

    fun updateCity(mode: Mode, city: City?) {
        when (mode) {
            Mode.START_CITY -> startCity.postValue(city)
            Mode.END_CITY -> endCity.postValue(city)
        }
    }

    fun clearLastSearch() {
        lastTem = null
    }
}