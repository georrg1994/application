package com.example.application.fragments

import android.content.res.Resources
import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import com.example.application.R
import com.example.application.adapters.CitiesAdapter
import com.example.application.helpers.MODE
import com.example.application.helpers.Mode
import com.example.application.helpers.SimpleClick
import com.example.application.models.Autocomplete
import com.example.application.presenters.getHint
import com.example.application.presenters.searchAutoCompleteSettings
import kotlinx.android.synthetic.main.fragment_search.*


class SearchCityFragment : TripBaseFragment(), SearchView.OnQueryTextListener {
    private lateinit var adapter: CitiesAdapter
    private var mode = Mode.START_CITY

    private val searchResultObserver = Observer<Autocomplete?> { autoComplete ->
        autoComplete?.let {
            nothingFound.visibility = if (it.cities.isNullOrEmpty() && !tripViewModel.lastTem.isNullOrEmpty())
                View.VISIBLE
            else
                View.GONE

            adapter.updateList(it.cities)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            mode = it.get(MODE) as Mode
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // title
        refreshActionBar(true)

        // init adapter
        adapter = CitiesAdapter(context, ArrayList(), getClickListenerByCity())
        recyclerView.adapter = adapter
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (savedInstanceState == null)
            tripViewModel.clearLastSearch()

        // observers
        tripViewModel.getAutocomplete().observe(this, searchResultObserver)
    }


    private fun getClickListenerByCity() = object : SimpleClick {
        override fun onItemClick(position: Int) {
            val city = adapter.getItem(position)
            tripViewModel.updateCity(mode, city)
            activity?.onBackPressed()
        }
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        initSearch(menu, inflater)
    }

    override fun onQueryTextSubmit(query: String) = onQueryTextChange(query)

    override fun onQueryTextChange(query: String): Boolean {
        tripViewModel.safeAutocompleteUpdate(query)
        return true
    }

    private fun initSearch(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.search_menu, menu)

        val searchMenuItem = menu.findItem(R.id.search)
        val searchView = searchMenuItem.actionView as SearchView
        val searchAutoComplete = searchView.findViewById<SearchView.SearchAutoComplete>(R.id.search_src_text)
        val hint = getHint(context, mode)

        searchAutoCompleteSettings(context, searchAutoComplete, hint)

        searchView.setOnQueryTextListener(this)
        searchView.onActionViewExpanded()
        searchView.maxWidth = Resources.getSystem().displayMetrics.widthPixels
        searchView.setQuery(tripViewModel.lastTem, true)
    }

    companion object {
        @JvmStatic
        fun newInstance(mode: Mode) =
            SearchCityFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(MODE, mode)
                }
            }
    }
}
