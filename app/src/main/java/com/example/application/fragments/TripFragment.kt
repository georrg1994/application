package com.example.application.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.example.application.R
import com.example.application.activities.MapsActivity
import com.example.application.helpers.AnimationsTypes
import com.example.application.helpers.Mode
import com.example.application.helpers.whenAllNotNull
import com.example.application.models.City
import kotlinx.android.synthetic.main.direction_fields.*
import kotlinx.android.synthetic.main.fragment_trip.*

class TripFragment : TripBaseFragment() {
    private val startCityObserver = Observer<City?> { startTextField.setText(it?.fullname) }
    private val endCityObserver = Observer<City?> { endTextField.setText(it?.fullname) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_trip, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // title
        refreshActionBar(getString(R.string.trip_choose_direction), false)

        // listeners
        startTextField.setOnClickListener { selectCity(Mode.START_CITY) }
        endTextField.setOnClickListener { selectCity(Mode.END_CITY) }
        searching.setOnClickListener { search() }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        // observers
        tripViewModel.startCity.observe(viewLifecycleOwner, startCityObserver)
        tripViewModel.endCity.observe(viewLifecycleOwner, endCityObserver)
    }

    private fun selectCity(mode: Mode) =
        replaceFragment(R.id.container, SearchCityFragment.newInstance(mode), AnimationsTypes.RIGHT, true)

    private fun search() {
        listOf(tripViewModel.startCity.value, tripViewModel.endCity.value, context).whenAllNotNull {
            MapsActivity.startMapActivity(context!!, tripViewModel.startCity.value!!, tripViewModel.endCity.value!!)
        }
    }
}
