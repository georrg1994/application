package com.example.application.fragments

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import com.example.application.viewmodels.TripViewModel

abstract class TripBaseFragment : BaseFragment() {
    protected lateinit var tripViewModel: TripViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (!::tripViewModel.isInitialized) {
            activity?.let {
                tripViewModel = ViewModelProviders.of(it).get(TripViewModel::class.java)
            }
        }
    }
}