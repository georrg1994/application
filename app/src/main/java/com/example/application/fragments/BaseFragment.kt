package com.example.application.fragments

import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.application.R
import com.example.application.helpers.AnimationsTypes
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.android.Main

abstract class BaseFragment : Fragment() {
    protected val uiScope = CoroutineScope(Dispatchers.Main + Job())

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home)
            activity?.onBackPressed()

        return super.onOptionsItemSelected(item)
    }

    protected fun refreshActionBar(isShowHome: Boolean) =
        refreshActionBar("", isShowHome)

    protected fun refreshActionBar(title: String, isShowHome: Boolean = false) {
        (activity as AppCompatActivity).supportActionBar?.title = title
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowHomeEnabled(isShowHome)
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(isShowHome)
    }

    protected fun replaceFragment(
        container: Int, fragment: Fragment,
        animationType: AnimationsTypes = AnimationsTypes.WITHOUT,
        saveInStack: Boolean = true
    ) {
        activity ?: return
        val tr = activity!!.supportFragmentManager.beginTransaction()

        when (animationType) {
            AnimationsTypes.LEFT ->
                tr.setCustomAnimations(
                    R.anim.left_animation_enter,
                    R.anim.left_animation_leave,
                    R.anim.right_animation_enter,
                    R.anim.right_animation_leave
                )
            AnimationsTypes.RIGHT ->
                tr.setCustomAnimations(
                    R.anim.right_animation_enter,
                    R.anim.right_animation_leave,
                    R.anim.left_animation_enter,
                    R.anim.left_animation_leave
                )
            else -> {
            }
        }

        tr.replace(container, fragment, fragment.tag)

        if (saveInStack)
            tr.addToBackStack(fragment.tag)

        tr.commitAllowingStateLoss()
    }
}