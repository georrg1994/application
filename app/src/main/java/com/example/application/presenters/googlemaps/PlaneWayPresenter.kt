package com.example.application.presenters.googlemaps

import android.content.Context
import com.example.application.R
import com.example.application.models.City
import com.google.android.gms.maps.model.*
import com.google.maps.android.SphericalUtil
import java.util.Arrays.asList


const val COUNT_OF_DOTES = 300

class PlaneWayPresenter {
    private val polyOptionsPattern = asList(Dot(), Gap(10f))
    private val wayPoints = ArrayList<LatLng>()

    fun getPolyline(startCity: City, endCity: City): PolylineOptions? {
        val polyLineOptions = PolylineOptions().apply {
            pattern(polyOptionsPattern)
        }

        startCity.location?.let { start ->
            endCity.location?.let { end ->

                val startLat = start.lat ?: 0.0
                val startLon = start.lon ?: 0.0
                val endLat = end.lat ?: 0.0
                val endLon = end.lon ?: 0.0

                val diffLat = (endLat - startLat) / 2.0
                val diffLon = (endLon - startLon) / 2.0
                val centerLat = startLat + diffLat          // point in the middle of cities (lat)
                val centerLon = startLon + diffLon          // point in the middle of cities (lon)

                val offset = getOffset(diffLat, diffLon)

                polyLineOptions.gmapsCubicBezier(
                    LatLng(startLat, startLon),                       // start ciry
                    LatLng(endLat, endLon),                           // end city
                    LatLng(centerLat + offset, centerLon + offset),   // reference point A
                    LatLng(centerLat - offset, centerLon - offset)    // reference point B
                )
            }
        }

        return polyLineOptions
    }

    /**
     * Calculation method for reference wayPoints
     */
    private fun getOffset(diffLat: Double, diffLon: Double) =
        if (Math.abs(diffLat) > Math.abs(diffLon))
            diffLat / 2
        else
            diffLon / 2

    /**
     * Create array of wayPoints for way and plane
     */
    private fun PolylineOptions.gmapsCubicBezier(p1: LatLng, p2: LatLng, pA: LatLng, pB: LatLng) {

        //Polyline options
        var curveLatLng: LatLng?
        val step = 1.0 / COUNT_OF_DOTES
        var t = 0.0
        while (t < 1.0 + step) {
            // P = (1−t)3P1 + 3(1−t)2tP2 +3(1−t)t2P3 + t3P4; for 4 wayPoints
            val arcX = ((1 - t) * (1 - t) * (1 - t) * p1.latitude
                    + 3.0 * (1 - t) * (1 - t) * t * pA.latitude
                    + 3.0 * (1 - t) * t * t * pB.latitude
                    + t * t * t * p2.latitude)

            val arcY = ((1 - t) * (1 - t) * (1 - t) * p1.longitude
                    + 3.0 * (1 - t) * (1 - t) * t * pA.longitude
                    + 3.0 * (1 - t) * t * t * pB.longitude
                    + t * t * t * p2.longitude)

            curveLatLng = LatLng(arcX, arcY)
            wayPoints.add(curveLatLng)
            this.add(curveLatLng)

            t += step
        }

        wayPoints.add(p2)
        this.add(p2)
    }

    fun getPlaneMarker(context: Context?): MarkerOptions? {
        context?.let {
            val bitmapDescriptor = getBitmapDescriptorFromDrawableRes(context, R.drawable.ic_plane)
            val bearing = SphericalUtil.computeHeading(wayPoints[0], wayPoints[1]).toFloat()
            return MarkerOptions().position(wayPoints[0]).icon(bitmapDescriptor).zIndex(1f).rotation(bearing)
        }

        return null
    }

    fun updatePlaneMarker(marker: Marker?, index: Int) {
        marker ?: return

        if (index < COUNT_OF_DOTES) {
            marker.position = wayPoints[index]

            if (index + 10 < COUNT_OF_DOTES)
                marker.rotation = SphericalUtil.computeHeading(wayPoints[index], wayPoints[index + 10]).toFloat()
        }
    }
}