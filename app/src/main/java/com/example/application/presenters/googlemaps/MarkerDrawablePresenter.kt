package com.example.application.presenters.googlemaps

import android.graphics.*
import com.example.application.models.City
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

fun getCityMarker(city: City): MarkerOptions? {
    return if (city.location != null) {
        val text = if (city.iata.isNotEmpty())
            city.iata[0]
        else
            city.countryCode

        val bitmapDescriptor = BitmapDescriptorFactory.fromBitmap(getBitmap(text))
        MarkerOptions().position(LatLng(city.location!!.lat!!, city.location!!.lon!!)).title(city.city)
            .icon(bitmapDescriptor)
    } else
        null
}

private fun getBitmap(text: String?, textSize: Float = 52f): Bitmap {
    val paint = Paint()

    // general
    paint.style = Paint.Style.FILL
    paint.alpha = 150
    paint.isAntiAlias = true
    paint.textSize = textSize
    paint.textAlign = Paint.Align.CENTER
    paint.typeface = Typeface.create(Typeface.DEFAULT, Typeface.BOLD)

    // calculations
    val bounds = paint.getHeightOfText(text)
    val rectOffset = bounds.height()
    val height = bounds.height() + rectOffset
    val width = bounds.width() + 1.2f * rectOffset
    val offset = 0.02f * width

    val bitmap = Bitmap.createBitmap(width.toInt(), height, Bitmap.Config.ARGB_8888)
    val canvas = Canvas(bitmap)

    // draw
    paint.color = Color.WHITE
    canvas.customDrawRoundRect(0f, 0f, width, height.toFloat(), paint)        // white background

    paint.color = Color.parseColor("#FECB83")
    canvas.customDrawRoundRect(offset, offset, width - offset, height - offset, paint)      // less yellow background

    text?.let {
        paint.color = Color.WHITE
        canvas.drawText(text, width / 2f, (height + bounds.height()) / 2f, paint)    // text
    }

    return bitmap
}

/**
 * This function allowed to create rectangles with rounded corners
 */
private fun Canvas.customDrawRoundRect(
    left: Float,
    top: Float,
    right: Float,
    bottom: Float,
    paint: Paint
) {
    val radius = (bottom - top) / 2
    val path = Path()
    path.moveTo(left, top)
    path.lineTo(right - radius, top)
    path.quadTo(right, top, right, top + radius)
    path.lineTo(right, bottom - radius)
    path.quadTo(right, bottom, right - radius, bottom)
    path.lineTo(left + radius, bottom)
    path.quadTo(left, bottom, left, bottom - radius)
    path.lineTo(left, top + radius)
    path.quadTo(left, top, left + radius, top)

    this.drawPath(path, paint)
}

private fun Paint.getHeightOfText(text: String?): Rect {
    val bounds = Rect()

    text ?: return bounds

    this.getTextBounds(text, 0, text.length, bounds)
    return bounds
}