package com.example.application.presenters.googlemaps

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import com.example.application.models.City
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds


fun getBitmapDescriptorFromDrawableRes(context: Context, @DrawableRes resId: Int): BitmapDescriptor? {
    val drawable = ContextCompat.getDrawable(context, resId)

    drawable?.let {
        drawable.setBounds(0, 0, drawable.intrinsicWidth, drawable.intrinsicHeight)
        val bitmap = Bitmap.createBitmap(drawable.intrinsicWidth, drawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        drawable.draw(canvas)

        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    return null
}

fun GoogleMap.zoomWay(startCity: City, endCity: City) {
    val startLat = startCity.location?.lat ?: return
    val startLon = startCity.location?.lon ?: return
    val endLat = endCity.location?.lat ?: return
    val endLon = endCity.location?.lon ?: return

    val builder = getZoomBuilder(startLat, startLon, endLat, endLon)
    this.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 1))
}

private fun getZoomBuilder(
    lat1: Double,
    lon1: Double,
    lat2: Double,
    lon2: Double
): LatLngBounds.Builder {
    val builder = LatLngBounds.Builder()

    val firstIsLeft = lat1 <= lat2
    val firstIsRight = lat1 >= lat2
    val firstIsBottom = lon1 <= lon2
    val firstIsTop = lon1 >= lon2
    val offset = 0.1 * getMaxDiff(lat1, lon1, lat2, lon2)

    val zoom1: LatLng
    val zoom2: LatLng
    when {
        firstIsLeft && firstIsBottom -> {
            zoom1 = LatLng(lat1 - offset, lon1 - offset)
            zoom2 = LatLng(lat2 + offset, lon2 + offset)
        }
        firstIsLeft && firstIsTop -> {
            zoom1 = LatLng(lat1 - offset, lon1 + offset)
            zoom2 = LatLng(lat2 + offset, lon2 - offset)
        }
        firstIsRight && firstIsBottom -> {
            zoom1 = LatLng(lat1 + offset, lon1 - offset)
            zoom2 = LatLng(lat2 - offset, lon2 + offset)
        }
        firstIsRight && firstIsTop -> {
            zoom1 = LatLng(lat1 + offset, lon1 + offset)
            zoom2 = LatLng(lat2 - offset, lon2 - offset)
        }
        else -> {
            zoom1 = LatLng(lat1, lon1)
            zoom2 = LatLng(lat2, lon2)
        }
    }

    builder.include(zoom1)
    builder.include(zoom2)

    return builder
}

fun getMaxDiff(
    lat1: Double,
    lon1: Double,
    lat2: Double,
    lon2: Double
): Double {
    val diffLat = Math.abs(lat2 - lat1)
    val diffLon = Math.abs(lon2 - lon1)
    return maxOf(diffLat, diffLon)
}