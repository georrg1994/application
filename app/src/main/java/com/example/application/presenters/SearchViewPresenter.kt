package com.example.application.presenters

import android.content.Context
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import com.example.application.R
import com.example.application.helpers.Mode

fun searchAutoCompleteSettings(
    context: Context?, searchAutoComplete: SearchView.SearchAutoComplete,
    hintText: String
) {
    // set colors and hint text
    context?.let {
        searchAutoComplete.setHintTextColor(ContextCompat.getColor(it, R.color.white))
        searchAutoComplete.setTextColor(ContextCompat.getColor(it, R.color.white))
        searchAutoComplete.hint = hintText
        searchAutoComplete.isCursorVisible = true
    }
}

fun getHint(context: Context?, mode: Mode): String {
    context?.let {
        return when (mode) {
            Mode.START_CITY -> context.getString(R.string.search_hint_start_city)
            Mode.END_CITY -> context.getString(R.string.search_hint_end_city)
        }
    }

    return ""
}