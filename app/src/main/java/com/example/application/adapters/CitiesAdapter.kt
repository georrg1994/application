package com.example.application.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.application.helpers.SimpleClick
import com.example.application.models.City
import com.example.application.viewholders.CityViewHolder

class CitiesAdapter(
    context: Context?,
    private var items: ArrayList<City>,
    private val listener: SimpleClick
) : RecyclerView.Adapter<CityViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        CityViewHolder.onCreate(inflater, parent, listener)

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: CityViewHolder, position: Int) =
        holder.bindTo(items[position])

    fun updateList(fullList: List<City>?) {
        items.clear()

        if (!fullList.isNullOrEmpty())
            items.addAll(fullList)

        notifyDataSetChanged()
    }

    fun getItem(position: Int) = if (position < items.size)
        items[position]
    else
        null
}