package com.example.application.models

import com.google.gson.annotations.SerializedName

data class Hotel(
    @SerializedName("state") var state: Any? = null,
    @SerializedName("stars") var stars: Double? = null,
    @SerializedName("locationFullName") var locationFullName: String? = null,
    @SerializedName("latinLocationFullName") var latinLocationFullName: String? = null,
    @SerializedName("hotelFullName") var hotelFullName: String? = null,
    @SerializedName("location") var location: Location? = null,
    @SerializedName("timezone") var timezone: String? = null,
    @SerializedName("timezonesec") var timezonesec: Double? = null,
    @SerializedName("id") var id: Double? = null,
    @SerializedName("locationId") var locationId: Double? = null,
    @SerializedName("photoCount") var photoCount: Double? = null,
    @SerializedName("city") var city: String? = null,
    @SerializedName("latinCity") var latinCity: String? = null,
    @SerializedName("clar") var clar: String? = null,
    @SerializedName("latinClar") var latinClar: String? = null,
    @SerializedName("latinCountry") var latinCountry: String? = null,
    @SerializedName("locationHotelsCount") var locationHotelsCount: Any? = null,
    @SerializedName("rating") var rating: Double? = null,
    @SerializedName("country") var country: String? = null,
    @SerializedName("distance") var distance: Double? = null,
    @SerializedName("_score") var score: Double? = null,
    @SerializedName("name") var name: String? = null,
    @SerializedName("latinName") var latinName: String? = null,
    @SerializedName("address") var address: String? = null,
    @SerializedName("photos") var photos: List<Double>? = null,
    @SerializedName("countryId") var countryId: Double? = null
)