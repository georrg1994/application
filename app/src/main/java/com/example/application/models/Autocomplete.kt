package com.example.application.models

import com.google.gson.annotations.SerializedName

data class Autocomplete(
    @SerializedName("cities") var cities: List<City>? = null,
    @SerializedName("hotels") var hotels: List<Hotel>? = null
)