package com.example.application.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class City(
    @SerializedName("countryCode") var countryCode: String? = null,
    @SerializedName("country") var country: String? = null,
    @SerializedName("latinFullName") var latinFullName: String? = null,
    @SerializedName("fullname") var fullname: String? = null,
    @SerializedName("clar") var clar: String? = null,
    @SerializedName("latinClar") var latinClar: String? = null,
    @SerializedName("location") var location: Location? = null,
    @SerializedName("hotelsCount") var hotelsCount: Double? = null,
    @SerializedName("iata") var iata: List<String> = ArrayList(),
    @SerializedName("city") var city: String? = null,
    @SerializedName("latinCity") var latinCity: String? = null,
    @SerializedName("timezone") var timezone: String? = null,
    @SerializedName("timezonesec") var timezonesec: Double? = null,
    @SerializedName("latinCountry") var latinCountry: String? = null,
    @SerializedName("id") var id: Double? = null,
    @SerializedName("countryId") var countryId: Double? = null,
    @SerializedName("_score") var score: Double? = null,
    @SerializedName("state") var state: Any? = null
) : Parcelable {
    constructor(parcel: Parcel) : this() {
        countryCode = parcel.readString()
        country = parcel.readString()
        latinFullName = parcel.readString()
        fullname = parcel.readString()
        clar = parcel.readString()
        latinClar = parcel.readString()
        location = parcel.readParcelable(Location::class.java.classLoader)
        hotelsCount = parcel.readDouble()
        parcel.readList(iata, String::class.java.classLoader)
        city = parcel.readString()
        latinCity = parcel.readString()
        timezone = parcel.readString()
        timezonesec = parcel.readDouble()
        latinCountry = parcel.readString()
        id = parcel.readDouble()
        countryId = parcel.readDouble()
        score = parcel.readDouble()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(countryCode)
        parcel.writeString(country)
        parcel.writeString(latinFullName)
        parcel.writeString(fullname)
        parcel.writeString(clar)
        parcel.writeString(latinClar)
        parcel.writeParcelable(location, flags)
        parcel.writeDouble(hotelsCount ?: 0.0)
        parcel.writeList(iata)
        parcel.writeString(city)
        parcel.writeString(latinCity)
        parcel.writeString(timezone)
        parcel.writeDouble(timezonesec ?: 0.0)
        parcel.writeString(latinCountry)
        parcel.writeDouble(id ?: 0.0)
        parcel.writeDouble(countryId ?: 0.0)
        parcel.writeDouble(score ?: 0.0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<City> {
        override fun createFromParcel(parcel: Parcel): City {
            return City(parcel)
        }

        override fun newArray(size: Int): Array<City?> {
            return arrayOfNulls(size)
        }
    }

}
