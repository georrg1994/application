package com.example.application.retrofit

import com.example.application.models.Autocomplete
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("/autocomplete")
    fun getAutocomplete(
        @Query("term") term: String,
        @Query("lang") lang: String
    ): Call<Autocomplete>
}