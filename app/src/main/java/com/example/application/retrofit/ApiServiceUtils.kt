package com.example.application.retrofit

const val GRAPH_API_URL = "https://yasen.hotellook.com"

fun getApiService(baseUrl: String) =
    RetrofitClient.getClient(baseUrl).create(ApiService::class.java)!!