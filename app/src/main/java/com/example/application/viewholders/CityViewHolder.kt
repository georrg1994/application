package com.example.application.viewholders

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.application.R
import com.example.application.helpers.SimpleClick
import com.example.application.models.City
import kotlinx.android.synthetic.main.item_city.view.*

class CityViewHolder private constructor(
    private val view: View,
    private val listener: SimpleClick
) : RecyclerView.ViewHolder(view) {

    @SuppressLint("SetTextI18n")
    fun bindTo(city: City) {
        view.title.text = city.fullname
        view.code.text = city.countryCode

        // listener
        view.setOnClickListener { listener.onItemClick(adapterPosition) }
    }

    companion object {
        fun onCreate(
            inflater: LayoutInflater, viewGroup: ViewGroup,
            listener: SimpleClick
        ): CityViewHolder {
            val view = inflater.inflate(R.layout.item_city, viewGroup, false)
            return CityViewHolder(view, listener)
        }
    }
}