package com.example.application.repositories

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.example.application.helpers.LoadingState
import com.example.application.models.Autocomplete
import com.example.application.retrofit.GRAPH_API_URL
import com.example.application.retrofit.getApiService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TripRepository(private val loadingState: MutableLiveData<LoadingState>) {
    private val autocomplete: MutableLiveData<Autocomplete> = MutableLiveData()

    fun getAutocomplete(term: String, lang: String): MutableLiveData<Autocomplete> {
        if (term.isEmpty()) {
            autocomplete.postValue(Autocomplete())
            return autocomplete
        }

        loadingState.postValue(LoadingState.LOADING)

        getApiService(GRAPH_API_URL).getAutocomplete(term, lang).enqueue(object : Callback<Autocomplete> {
            override fun onFailure(call: Call<Autocomplete>, t: Throwable) {
                loadingState.postValue(LoadingState.FAILED)
                autocomplete.postValue(null)
            }

            override fun onResponse(call: Call<Autocomplete>, response: Response<Autocomplete>) {
                loadingState.postValue(LoadingState.SUCCESSFUL)
                autocomplete.postValue(response.body())
            }
        })

        return autocomplete
    }

    companion object {
        private var repository: TripRepository? = null

        fun getInstance(loadingState: MutableLiveData<LoadingState>): TripRepository {
            synchronized(Application::class.java) {
                if (repository == null)
                    repository = TripRepository(loadingState)
            }

            return repository!!
        }
    }
}