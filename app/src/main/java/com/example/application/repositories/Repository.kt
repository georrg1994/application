package com.example.application.repositories

import androidx.lifecycle.MutableLiveData
import com.example.application.helpers.LoadingState

/**
 * Facade for requests
 */
object Repository {
    val loadingState: MutableLiveData<LoadingState> = MutableLiveData()
    private val tripRepository = TripRepository.getInstance(loadingState)

    /**
     * Trips requests
     */
    fun getAutocomplete(term: String, lang: String = "en") = tripRepository.getAutocomplete(term, lang)
}