package com.example.application.helpers

interface SimpleClick {
    fun onItemClick(position: Int)
}