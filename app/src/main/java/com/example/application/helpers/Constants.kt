package com.example.application.helpers

const val START_CITY = "START_CITY"
const val END_CITY = "END_CITY"
const val MODE = "MODE"

enum class Mode { START_CITY, END_CITY }
enum class AnimationsTypes { LEFT, RIGHT, WITHOUT }
enum class LoadingState { LOADING, SUCCESSFUL, FAILED }