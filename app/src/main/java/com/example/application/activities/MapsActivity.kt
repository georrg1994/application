package com.example.application.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.application.R
import com.example.application.helpers.END_CITY
import com.example.application.helpers.START_CITY
import com.example.application.models.City
import com.example.application.presenters.googlemaps.COUNT_OF_DOTES
import com.example.application.presenters.googlemaps.PlaneWayPresenter
import com.example.application.presenters.googlemaps.getCityMarker
import com.example.application.presenters.googlemaps.zoomWay
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.Marker
import kotlinx.android.synthetic.main.activity_maps.*
import kotlinx.coroutines.*
import kotlinx.coroutines.android.Main

private const val ANIMATION_DURATION = 10 * 1000L

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var googleMap: GoogleMap
    private lateinit var startCity: City
    private lateinit var endCity: City
    private var marker: Marker? = null
    private var planeWayPresenter = PlaneWayPresenter()

    private val uiScope = CoroutineScope(Dispatchers.Main + Job())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        (mapFragment as SupportMapFragment).getMapAsync(this)

        startCity = intent.getParcelableExtra(START_CITY)
        endCity = intent.getParcelableExtra(END_CITY)

        startAnimation()
    }

    override fun onMapReady(gm: GoogleMap) {
        googleMap = gm

        with(googleMap) {
            uiSettings.isRotateGesturesEnabled = false

            // markers of cities
            addMarker(getCityMarker(startCity))
            addMarker(getCityMarker(endCity))

            // way of trip
            addPolyline(planeWayPresenter.getPolyline(startCity, endCity))

            // plane
            marker = addMarker(planeWayPresenter.getPlaneMarker(this@MapsActivity))

            zoomWay(startCity, endCity)
        }
    }

    private fun startAnimation() {
        uiScope.launch {
            val delayStep = ANIMATION_DURATION / COUNT_OF_DOTES
            for (i in 0..COUNT_OF_DOTES) {
                planeWayPresenter.updatePlaneMarker(marker, i)
                delay(delayStep)
            }
        }
    }

    companion object {
        fun startMapActivity(context: Context, start: City, end: City) {
            val intent = Intent(context, MapsActivity::class.java).apply {
                putExtra(START_CITY, start)
                putExtra(END_CITY, end)
            }

            context.startActivity(intent)
        }
    }
}
