package com.example.application.activities

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.application.R
import com.example.application.fragments.TripFragment
import com.example.application.helpers.LoadingState
import com.example.application.helpers.reobserve
import com.example.application.viewmodels.BaseViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var viewModel: BaseViewModel

    private val loadingStateObserver = Observer<LoadingState> {
        when (it) {
            LoadingState.LOADING -> waitProgressbar.visibility = View.VISIBLE
            LoadingState.SUCCESSFUL -> waitProgressbar.visibility = View.GONE
            LoadingState.FAILED, null -> {
                waitProgressbar.visibility = View.GONE
                Snackbar.make(container, "loading error", Snackbar.LENGTH_SHORT).setDuration(1000).show()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (!::viewModel.isInitialized)
            viewModel = ViewModelProviders.of(this).get(BaseViewModel::class.java)

        viewModel.loadingState.reobserve(this, loadingStateObserver)

        if (savedInstanceState == null)
            setMainContainer(TripFragment())
    }

    private fun setMainContainer(fragment: Fragment) =
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, fragment)
            .commitAllowingStateLoss()
}
